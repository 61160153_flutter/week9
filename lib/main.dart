import 'package:flutter/material.dart';
import 'cat.dart';
import 'cat_dao.dart';
import 'dog.dart';
import 'dog_dao.dart';

void main() async {
  var fido = Dog(id: 0, name: 'Fido', age: 35);
  var dido = Dog(id: 1, name: 'Dido', age: 20);
  await DogDao.insertDog(fido);
  await DogDao.insertDog(dido);

  print(await DogDao.dogs());

  fido = Dog(
    id: fido.id,
    name: fido.name,
    age: fido.age + 7,
  );

  await DogDao.updateDog(fido);
  print(await DogDao.dogs());

  await DogDao.deleteDog(0);
  print(await DogDao.dogs());

  /*cat*/

  var tiger = Cat(id: 0, name: 'tiger', age: 5);
  var bamboo = Cat(id: 1, name: 'bamboo', age: 3);
  await CatDao.insertCat(tiger);
  await CatDao.insertCat(bamboo);

  print(await CatDao.cats());

  tiger = Cat(
    id: tiger.id,
    name: tiger.name,
    age: tiger.age + 3,
  );

  await CatDao.updateCat(tiger);
  print(await CatDao.cats());

  await CatDao.deleteCat(0);
  print(await CatDao.cats());
}
